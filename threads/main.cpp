#include "threads.h"
#include <iostream>

int* makeRandomArr(int min, int max, int size);

int main()
{
	call_I_Love_Threads();

	std::vector<int> primes1;
	getPrimes(58, 100, primes1);

	//printVector(callGetPrimes(0, 1000));

	//std::vector<int> primes3 = callGetPrimes(0, 1000000);

	printVector(primes1);

	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 2);
	
	system("pause");
	system("CLS");
	threadsColors(10);
	system("pause");

	system("CLS");
	recursiveThreads(50);
	system("pause");

	system("CLS");
	std::cout << "The max is: " << maxMultipleThreads(makeRandomArr(0, 1000, 1000), 1000, 10) << std::endl;
	system("pause");

	return 0;
}

int* makeRandomArr(int min, int max, int size)
{
	int* arr = new int[size];
	for (int i = 0; i < size; i++)
	{
		*(arr + i) = rand() % (max - min) + min;
	}
	return arr;
}