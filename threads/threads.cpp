#include "threads.h"
#include <Windows.h>
#include <thread>

enum colors1{RED = 31, GREEN, YELLOW, BLUE, MAGENTA, CYAN, LG};
enum colors2{DG = 90, LR, LGREEN, LY, LB, LM, LC, WHITE};


void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	t.join();
}

void printVector(std::vector<int> primes)
{
	for (unsigned int i = 0; i < primes.size(); i++)
	{
		std::cout << primes[i] << std::endl;
	}
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	bool prime = true;
	for (int i = begin; i <= end; i++)
	{
		for (int j = 2; j <= std::sqrt(i) && prime; j++)
		{
			if (i % j == 0) prime = false;
		}
		if (prime) primes.push_back(i);
		prime = true;
	}
}

std::vector<int> callGetPrimes(int begin, int end)
{
	std::chrono::high_resolution_clock::time_point start_t = std::chrono::high_resolution_clock::now();
	std::vector<int> primes;
	std::thread t(getPrimes, begin, end, std::ref(primes));
	std::chrono::high_resolution_clock::time_point end_t = std::chrono::high_resolution_clock::now();
	t.join();
	std::cout << "Time: " << std::chrono::duration<double, std::milli>(end_t - start_t).count() << std::endl;
	return primes;
}


void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	bool prime = true;
	if (file.is_open()) //checking if the file is open
	{
		for (int i = begin; i <= end; i++)
		{
			for (int j = 2; j <= std::sqrt(i) && prime; j++) //checking if i divides more numbers (checking prime number)
			{
				if (i % j == 0) prime = false;
			}
			if (prime) file << i << std::endl; //writing to file if the number is prime
			prime = true;
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file(filePath);
	if (file.is_open())
	{
		std::chrono::high_resolution_clock::time_point start_t = std::chrono::high_resolution_clock::now();
		for (int i = 1; i <= N; i++)
		{
			std::thread t(writePrimesToFile, begin + ((end - begin) /N) * (i - 1) , begin + ((end - begin) / N) * i, std::ref(file));
			t.join();
		}
		std::chrono::high_resolution_clock::time_point end_t = std::chrono::high_resolution_clock::now();
		std::cout << "Time: " << std::chrono::duration<double, std::milli>(end_t - start_t).count() << std::endl;
		file.close();
	}
	else std::cerr << "Could not open file!" << std::endl;
}

void printColor(int color, int num)
{
	for (int i = 0; i < num; i++) std::cout << "\033[" << color <<"mColor\033[0m" << std::endl;
}

void threadsColors(int num)
{
	std::thread tRed(printColor, RED, num);
	tRed.join();
	std::thread tGreen(printColor, GREEN, num);
	tGreen.join();
	std::thread tYellow(printColor, YELLOW, num);
	tYellow.join();
	std::thread tBlue(printColor, BLUE, num);
	tBlue.join();
	std::thread tMagenta(printColor, MAGENTA, num);
	tMagenta.join();

	std::thread tCyan(printColor, CYAN, num);
	tCyan.join();
	std::thread tLG(printColor, LG, num);
	tLG.join();
	std::thread tDG(printColor, DG, num);
	tDG.join();
	std::thread tLR(printColor, LR, num);
	tLR.join();
	std::thread tLGREEN(printColor, LGREEN, num);
	tLGREEN.join();

	std::thread tLY(printColor, LY, num);
	tLY.join();
	std::thread tLB(printColor, LB, num);
	tLB.join();
	std::thread tLM(printColor, LM, num);
	tLM.join();
	std::thread tLC(printColor, LC, num);
	tLC.join();
	std::thread tWhite(printColor, WHITE, num);
	tWhite.join();
}

void printThread(int num)
{
	std::cout << "Hello from thread " << num << std::endl;
}

void recursiveThreads(int num)
{
	if (num >= 1)
	{
		std::thread t(recursiveThreads, num - 1);
		t.join();
		printThread(num);
	}
}

void getMax(int* arr, int size, int& max)
{
	for (int i = 1; i < size; i++)
	{
		if (max < *(arr + i)) max = *(arr + i);
	}
}

int maxMultipleThreads(int* arr, int size, int N)
{
	int max = *(arr);
	for (int i = 0; i < N; i++)
	{
		std::thread t(getMax, arr + ((size / N) * i), size / N, std::ref(max));
		t.detach();
	}
	return max;
}